// Định nghĩa state khởi tạo cho table
const initialState = {
    totalPrice: 0,
    iphoneQuantity: 0,
    rogTrixPhoneQuantity: 0,
    ssPhoneQuantity: 0,
    iPhoneName: "Iphone",
    rogTrixName: "Rogtrix Phone",
    sSName: "Samsung Phone",
    iPhonePrice: 900,
    rogTrixPrice: 650,
    sSPrice: 800,

}


const MobileOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case "IPHONE_BUY_CLICKED":
            state.iphoneQuantity = state.iphoneQuantity + 1;
            state.totalPrice = state.totalPrice + 900;
            console.log("Tên sản phẩm: " + state.iPhoneName);
            console.log("Giá sản phẩm: " + state.iPhonePrice);
            console.log("Số lượng sản phẩm: " + state.iphoneQuantity)
            break;

        case "ROGTRIX_BUY_CLICKED":
            state.rogTrixPhoneQuantity = state.rogTrixPhoneQuantity + 1;
            state.totalPrice = state.totalPrice + 650;
            console.log("Tên sản phẩm: " + state.rogTrixName);
            console.log("Giá sản phẩm: " + state.rogTrixPrice);
            console.log("Số lượng sản phẩm: " + state.rogTrixPhoneQuantity)


            break;
        case "SAMSUNG_BUY_CLICKED":
            state.ssPhoneQuantity = state.ssPhoneQuantity + 1;
            state.totalPrice = state.totalPrice + 800;
            console.log("Tên sản phẩm: " + state.sSName);
            console.log("Giá sản phẩm: " + state.sSPrice);
            console.log("Số lượng sản phẩm: " + state.ssPhoneQuantity)

            break;

        default:
            break;
    }
    return { ...state }
}

export default MobileOrderReducer;