//Root reducer
import { combineReducers } from "redux";

import MobileOrderReducer from "./mOderReducer";
//Tạo một root reducer:

const rootReducer = combineReducers({
    MobileOrderReducer
});

export default rootReducer