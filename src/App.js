import './App.css';
import Body from './components/Body';
import MobileOrder from './components/MobileOrder';

function App() {
  return (
    <div >
      <MobileOrder></MobileOrder>
      <Body />
    </div>
  );
}

export default App;
