import NotFound from "./components/404";
import IphoneDetail from "./components/ProductDetail.components/IphoneDetail";
import RogtrixDetail from "./components/ProductDetail.components/RogtrixDetail";
import SamsungDetail from "./components/ProductDetail.components/SamsungDetail";
const routes = [
    {
        path: "/",
        element: <></>
    },
    {
        path: "/iphone",
        element: <IphoneDetail />
    },
    {
        path: "/rogtrix",
        element: <RogtrixDetail />
    },
    {
        path: "/samsung",
        element: <SamsungDetail />
    },
    {
        path: "*",
        element: <NotFound />
    }

];

export default routes;