import { Button, Card, CardActions, CardContent, Container, Grid, Typography } from "@mui/material"
import { useSelector } from "react-redux";

const SamsungDetail = () => {
    // Khai báo hàm dispatch sự kiện tới redux store

    // useSelector để đọc state từ redux
    const { ssPhoneQuantity } = useSelector((reduxData) => {
        return reduxData.MobileOrderReducer;
    })

    return (

        <Container style={{ marginTop: "50px" }}>
            <Grid container spacing={10}>
                <Grid item md={4} >
                    <Card>
                        <CardContent>
                            <Typography>
                                Samsung Galaxy Z Fold 5
                            </Typography>
                            <Typography>
                                Price: 800 USD
                            </Typography>
                            <Typography>
                                Quantity: {ssPhoneQuantity}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button href="/" variant="contained" color="success" style={{ color: "orange" }}>
                                Back
                            </Button>

                        </CardActions>
                    </Card>
                </Grid>


            </Grid>

        </Container>


    )
}
export default SamsungDetail