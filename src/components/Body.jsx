import { Routes, Route } from "react-router-dom";
import routes from "../routes";

const Body = () => {
    return (
        <div>
            <Routes>
                {
                    routes.map((route, index) => {
                        return <Route key={index} path={route.path} element={route.element} />
                    })
                }
            </Routes>
        </div>
    )
}

export default Body;